# This topology contains a middle node connected via p2p and mpl

import mergexp as mx
from mergexp.net import capacity, latency, loss, routing, static, addressing, ipv4
from mergexp.unit import mbps, ms

net = mx.Topology('p2pplusmpl', routing == static, addressing == ipv4)

a = net.device('a')
b = net.device('b')
c = net.device('c')
d = net.device('d')

lan = net.connect([a,b,c], capacity == mbps(10), latency == ms(10))
lan.props['tags'] = ['lan']

lan[c].ip.addrs = ['10.0.0.1/24']
lan[a].ip.addrs = ['10.0.0.2/24']
lan[b].ip.addrs = ['10.0.0.3/24']

wan = net.connect([c,d], capacity == mbps(100), latency == ms(5))
wan.props['tags'] = ['wan']
wan[c].ip.addrs = ['10.0.1.1/24']
wan[d].ip.addrs = ['10.0.1.2/24']

mx.experiment(net)
