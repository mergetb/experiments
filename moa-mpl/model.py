import mergexp as mx
from mergexp.net import capacity, latency, loss
from mergexp.unit import mbps, ms

net = mx.Topology('moa-mpl')

a = net.device('a')
b = net.device('b')
c = net.device('c')
lan = net.connect([a, b, c], capacity == mbps(10), latency == ms(20))
lan.props['tags'] = ['lan']

lan[a].ip.addrs = ['10.0.0.1/24']
lan[b].ip.addrs = ['10.0.0.2/24']
lan[c].ip.addrs = ['10.0.0.3/24']

mx.experiment(net)
