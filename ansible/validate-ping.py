#!/usr/bin/env python3

import subprocess
import sys
import re

if len(sys.argv) < 3:
    print('usage: validate-ping.py <host> <latency>', file=sys.stderr)
    sys.exit(1)

target = sys.argv[1]
latency = float(sys.argv[2])

rtt_re = re.compile('rtt.*/([0-9.]+)/[0-9.]+/[0-9.]+ ms')

o = subprocess.run(["ping", "-q", "-c", "30", target], capture_output=True)
if o.returncode != 0:
    print('error: ping exited code %d' % o.returncode, file=sys.stderr)
    print(o.stdout)
    print(o.stderr, file=sys.stderr)
    sys.exit(1)
s = o.stdout.decode()
g = rtt_re.search(s)
avg = float(g.group(1)) / 2.0

# allow for a 5ms range around the expected average
if avg < latency-3.0 or avg > latency+3.0:
    print('expected latency of %f but got %f\n' % (latency, avg), file=sys.stderr)
    print(s, file=sys.stderr)
    sys.exit(1)

print('measured latency %fms' % avg)

sys.exit(0)
