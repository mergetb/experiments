#!/usr/bin/env bash

# run this on the XDC created by run-vte.sh

set -e
set -x

cd /tmp/moatest

. vars.local
. vars.common

xdc attach $USER $exp $realization

ansible-playbook -i hosts.${exp} -i hosts.common setup.yml
ansible-playbook -i hosts.${exp} -i hosts.common test-${exp}.yml
