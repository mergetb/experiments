#!/usr/bin/env python3

import sys
import subprocess
import os.path
import json

if len(sys.argv) < 3:
    print('''usage: %s <host> <loss>
loss:    specify the expected packet loss rate (0.0-1.0)
''' % (os.path.basename(sys.argv[0]),), file=sys.stderr)
    sys.exit(1)

target = sys.argv[1]
loss = float(sys.argv[2]) * 100.0

p = subprocess.run(['iperf3', '-c', target, '-u', '-b', '10M', '--json'], capture_output=True)
if p.returncode != 0:
    print('error: iperf3 exited code %d' % p.returncode, file=sys.stderr)
    print(p.stdout)
    print(p.stderr, file=sys.stderr)
    sys.exit(1)

o = json.loads(p.stdout.decode())
measured = float(o['end']['sum']['lost_percent'])

if measured < loss * 0.9 or measured > loss * 1.1:
    print('measured loss of %f%%, but expected %f%%' % (measured, loss), file=sys.stderr)
    sys.exit(1)

print('measured loss of %f' % loss)

sys.exit(0)
