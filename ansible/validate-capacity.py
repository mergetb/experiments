#!/usr/bin/env python3

import sys
import subprocess
import os.path
import json

if len(sys.argv) < 3:
    print('usage: %s <host> <capacity>' % (os.path.basename(sys.argv[0]),), file=sys.stderr)
    sys.exit(1)

target = sys.argv[1]
capacity = sys.argv[2].lower()

if capacity.endswith('mbps'):
    capacity = float(capacity[0:-4]) * 1000000
elif capacity.endswith('gbps'):
    capacity = float(capacity[0:-4]) * 1000000000
elif capacity.endswith('kbps'):
    capacity = float(capacity[0:-4]) * 1000
else:
    print('%s: unable to parse capacity' % capcity, file=sys.stderr)
    os.exit(1)

p = subprocess.run(['iperf3', '-c', target,'--json'], capture_output=True)
if p.returncode != 0:
    print(p.stderr, file=sys.stderr)
    os.exit(1)

o = json.loads(p.stdout.decode())
bitrate = float(o['end']['sum_sent']['bits_per_second'])

if bitrate < capacity * 0.9 or bitrate > capacity * 1.1:
    print('measured bitrate of %f, but expected %f' % (bitrate, capacity), file=sys.stderr)
    sys.exit(1)

print('measured bitrate of %f' % bitrate)

sys.exit(0)
