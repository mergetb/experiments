# This topology contains a middle node that has two mpl links

import mergexp as mx
from mergexp.net import capacity, latency, loss, routing, static, addressing, ipv4
from mergexp.unit import mbps, ms

net = mx.Topology('mpl-multi', routing == static, addressing == ipv4)

a = net.device('a')
b = net.device('b')
c = net.device('c')
d = net.device('d')
e = net.device('e')

lan1 = net.connect([a,b,c], capacity == mbps(10), latency == ms(20))
lan1.props['tags'] = ['lan_1']
lan2 = net.connect([c,d,e], capacity == mbps(100), latency == ms(10))
lan2.props['tags'] = ['lan_2']

lan1[c].ip.addrs = ['10.0.0.1/24']
lan1[a].ip.addrs = ['10.0.0.2/24']
lan1[b].ip.addrs = ['10.0.0.3/24']

lan2[c].ip.addrs = ['10.0.1.1/24']
lan2[d].ip.addrs = ['10.0.1.2/24']
lan2[e].ip.addrs = ['10.0.1.3/24']

mx.experiment(net)
