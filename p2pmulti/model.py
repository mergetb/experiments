# This topology contains a middle node that has two p2p links

import mergexp as mx
from mergexp.net import capacity, latency, loss, routing, static, addressing, ipv4
from mergexp.unit import mbps, ms

net = mx.Topology('p2pmulti', routing == static, addressing == ipv4)

a = net.device('a')
b = net.device('b')
c = net.device('c')

link1 = net.connect([a, b], capacity == mbps(10), latency == ms(20))
link1.props['tags'] = ['a_b']
link2 = net.connect([b, c], capacity == mbps(100), latency == ms(10))
link2.props['tags'] = ['b_c']

link1[a].ip.addrs = ['10.0.0.1/24']
link1[b].ip.addrs = ['10.0.0.2/24']

link2[b].ip.addrs = ['10.0.1.1/24']
link2[c].ip.addrs = ['10.0.1.2/24']

mx.experiment(net)
